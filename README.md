# Team A App2

This project contains a very simple app that can be used to demo the functionality of ArgoCD.

## Build App

* Make changes to the app i.e. modify `index.html` as needed
* Make changes to your values as needed in `charts/app2/values.yaml`
* Build and push container image

```sh
REPO=ttl.sh/$(uuidgen | tr '[:upper:]' '[:lower:]')
TAG=4h
podman build -t $REPO:$TAG .
podman push $REPO:$TAG

# Update your values to point to this image
repo="$REPO" yq '.image.repo = strenv(repo)' charts/app2/values.yaml --inplace
tag="$TAG" yq '.image.tag = strenv(tag)' charts/app2/values.yaml --inplace

# Update the chart version
VER=$(yq '.version' charts/app2/Chart.yaml)
VER=$(($VER + 1))
ver="$VER" yq '.version = strenv(ver)' charts/app2/Chart.yaml --inplace
```

## Publish Chart

```sh
helm package charts/app2
# Use the output of the following command to update the ArgoCD Application chart reference
helm push app2-$VER.tgz oci://$REPO 2>&1 | grep Pushed | sed s/Pushed://g
```

## Save Changes

```sh
git add index.yaml              # if changed
git add charts/app2/Chart.yaml  # if changed
git commit -m "updates app"
git push
```
