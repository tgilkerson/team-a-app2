package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

// healthz will respond with healthz
func healthz(w http.ResponseWriter, req *http.Request) {
	log.Println("request /healthz")
	fmt.Fprintf(w, "ok\n")
}

// headers will echo the request headers in the response
func headers(w http.ResponseWriter, req *http.Request) {

	// This handler does something a little more
	// sophisticated by reading all the HTTP request
	// headers and echoing them into the response body.
	for name, headers := range req.Header {
		for _, h := range headers {
			fmt.Fprintf(w, "%v: %v\n", name, h)
		}
	}
}

// root will respond with root content
func root(w http.ResponseWriter, req *http.Request) {

	now := fmt.Sprintf("%v", time.Now())

	htmlDoc := `
	  <!DOCTYPE html>
		<html>
		<head>
		<!--style>body {text-align: center;}</style-->
		<title>Team-A App2</title>
		</head>
			<body>
				<h1>Team-A App2</h1>
				<hr>
				<table>
					<tr>
						<td>Version</td>
						<td>%v</td>
					</tr>
					<tr>
						<td>Greeting</td>
						<td>%v</td>
					</tr>
					<tr>
						<td>Namespace</td>
						<td>%v</td>
					</tr>
					<tr>
						<td>Pod</td>
						<td>%v</td>
					</tr>
					<tr>
						<td>Time</td>
						<td>%v</td>
					</tr>
				</table>
				<hr>
				<p>
					<b>Heading</b> is set at development-time <br>
					<b>Version</b> is set at build-time (CI)<br>
					<b>Greeting</b> is set at deploy-time (CD)<br>
					<b>Namespace</b>,<b>Pod</b> and <b>Time</b> are set at run-time
				</p>
			</body>
		</html>`

	fmt.Fprintf(w, htmlDoc,
		os.Getenv("VERSION"),
		os.Getenv("GREETING"),
		os.Getenv("NAMESPACE"),
		os.Getenv("POD"),
		now)

}

// main
func main() {
	// Log to the console with date, time and filename prepended
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	http.HandleFunc("/", root)
	http.HandleFunc("/healthz", healthz)

	log.Println("Listening on 0.0.0.0:8080")
	http.ListenAndServe("0.0.0.0:8080", nil)

}
