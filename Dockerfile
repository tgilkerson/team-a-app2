#
# Dev
#
FROM golang:1.20 AS build

COPY ./cmd /app
COPY go.mod /app
WORKDIR /app
RUN go get ./...
RUN go build -o bin/app2 ./www/ 

#
# Prod
#
FROM gcr.io/distroless/base-debian11 as prod
COPY --from=build /app/bin/app2 /usr/local/bin/app2

ENTRYPOINT ["app2"]
EXPOSE 8080